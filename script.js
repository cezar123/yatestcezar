// array that holds items
var items = [];
var initialValue = '';

// line through item
$(document).on('click', '.complete', function () {
    var self = $(this);
    if(self.is(':checked')){
        self.closest('.row').find('.item').css('text-decoration', 'line-through');
    } else{
        self.closest('.row').find('.item').css('text-decoration', 'none');
    }
});

// edit item and show details
$(document).on('click', '.item', function () {
    var self = $(this);

    // store initial value
    initialValue = self.val();

    // edit item
    self.removeAttr('readonly');

    // show details
    var details = $('#details');
    var title = self.val() + ' details';
    details.find('.card-header').text(title);
    details.show();
});


// pressing enter stores the value
$(document).on('keypress', '.item', function (e) {
    var key = e.which;
    if(key == 13){
        var self = $(this);
        self.attr('readonly', 'readonly');
        var newValue = $.trim(self.val());

        // if value not changed do nothing
        if(initialValue && initialValue === newValue)
            return false;

        // remove old item from array
        if(initialValue)
            removeItem(initialValue);

        // add new item to array
        items.push(newValue);

        self.focusout();

        // send items array to server?

        return false;
    }


});

// display remove button
$(document).on('mouseenter', '.list-group-item', function(){
    $(this).find('.fas').show();
});

// hide remove button
$(document).on('mouseleave', '.list-group-item', function(){
    $(this).find('.fas').hide();
});

// removing item
$(document).on('click', '.fas', function(){
    var listItem = $(this).closest('.list-group-item');
    var itemToRemove = listItem.find('.item').val();
    removeItem(itemToRemove);
    listItem.remove();
});


// add item
function addItem(){
    var html = '   <li class="list-group-item">\n' +
        '                        <div class="row justify-content-center align-items-center">\n' +
        '                            <div class="col-sm-3">\n' +
        '                                <input type="checkbox" class="form-control complete">\n' +
        '                            </div>\n' +
        '                            <div class="col-sm-7">\n' +
        '                                <input type="text" class="form-control-plaintext item" value="">\n' +
        '                            </div>\n' +
        '                            <div class="col-sm-2">\n' +
        '                                <i class="fas fa-ban"></i>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </li>';

    $('.list-group').append(html);
    console.log($('.item').last());
    $('.item').last().focus();
}


function removeItem(item){
    items.splice($.inArray(item, items),1);
    // send items array to server
}

